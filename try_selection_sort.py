def selection_sort(data):
    n = len(data)
    sorted_data = data.copy()
    for k in range(n - 1, 0, -1):
        max_index = 0
        for i in range(1, k + 1):
            if sorted_data[i] > sorted_data[max_index]:
                max_index = i
        sorted_data[k], sorted_data[max_index] = sorted_data[max_index], sorted_data[k]

    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("Enter Numbers : ").split()))
    sorted_data = selection_sort(data)
    print(sorted_data)
