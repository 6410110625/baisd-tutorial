def coin_change_gd(change, coins):
    change_stat = dict()

    for c in reversed(sorted(coins)):
        change_stat[c] = change // c
        change = change % c

    return change_stat, sum(change_stat.values())


def coin_change_dp(change, coins):
    change_stat = dict()
    new_change = change % 10
    memo = [None for x in range(new_change + 1)]
    memo[0] = []
    ten = change // 10

    for i in range(1, new_change + 1):
        for coin in coins:
            if coin > i:
                continue
            elif not memo[i] or len(memo[i - coin]) + 1 < len(memo[i]):
                if memo[i - coin] != None:
                    memo[i] = memo[i - coin][:]
                    memo[i].append(coin)

    for i in range(len(coins) - 1):
        change_stat[coins[i]] = memo[-1].count(coins[i])

    change_stat[10] = ten
    res = dict(reversed(list(change_stat.items())))

    if memo[-1] != None:
        return res, sum(res.values())
    else:
        return "fail"


if __name__ == "__main__":
    coins = [1, 2, 4, 5, 10]
    print(coin_change_gd(38, coins))
    print(coin_change_dp(38, coins))
    coin_change_dp(38, coins)
