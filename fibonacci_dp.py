import timeit


def fibo_no_memo(n):
    f0 = 0
    f1 = 1
    if n < 2:
        return n
    return fibo_no_memo(n - 1) + fibo_no_memo(n - 2)


def fibo_memo(n):
    f = [0, 1, 1]
    for i in range(2, n + 1):
        f[2] = f[0] + f[1]
        f[0] = f[1]
        f[1] = f[2]
    return f[2]


if __name__ == "__main__":
    n = int(input("n = "))
    print(f"Without memo f({n}) = {fibo_no_memo(n)}", timeit.timeit(globals=globals()))
    print(f"With memo f({n}) = {fibo_memo(n)}", timeit.timeit(globals=globals()))
